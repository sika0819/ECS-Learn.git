﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public EntityManager entityManager;
    public EntityArchetype entityArchetype;

    public int createPrefabCout;
    public GameObject prefab;
    Mesh mesh;
    public Material[] materials;
    public Transform centerTrans;
    // Use this for initialization
    void Start () {
        mesh = prefab.GetComponent<MeshFilter>().sharedMesh;
        entityManager = World.Active.GetOrCreateManager<EntityManager>();
        NativeArray<Entity> entities = new NativeArray<Entity>(createPrefabCout, Allocator.Temp);
        entityManager.Instantiate(prefab, entities);
        
        for (int i = 0; i < createPrefabCout; i++)
        {
           
            //设置组件
            entityManager.SetComponentData(entities[i], new Position { Value = UnityEngine.Random.insideUnitSphere * 100 });
            entityManager.SetComponentData(entities[i], new Rotation { Value = quaternion.identity });
            float randRadius = UnityEngine.Random.Range(2, 1000);
            float randOmega = UnityEngine.Random.Range(-10, 10);
            float randTheta = UnityEngine.Random.Range(0, 360);
            float randp= UnityEngine.Random.Range(0, 360);
            entityManager.SetComponentData(entities[i], new MovementData
            {
                center = centerTrans.position,
                omega = randOmega,
                radius = randRadius,
                theta = randTheta,
                p=randp
            });
            int colorIndex = UnityEngine.Random.Range(0, 3);
            Debug.Log(colorIndex);
            //添加并设置组件
            entityManager.AddSharedComponentData(entities[i], new MeshInstanceRenderer
            {
                mesh = this.mesh,
                material = this.materials[colorIndex],
            });
        }
        entities.Dispose();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnDestroy()
    {
        
    }
}
