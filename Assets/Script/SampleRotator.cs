﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.Entities;
using Unity.Transforms;
using Unity.Collections;
using Unity.Rendering;
using Random = UnityEngine.Random;

public class SampleRotator : MonoBehaviour
{

    public EntityManager entityManager;
    public EntityArchetype entityArchetype;

    public int createPrefabCout;
    public GameObject prefab;
    public Mesh mesh;
    public Material material;

    // Use this for initialization
    void Start()
    {
        //创建实体管理器
        entityManager = World.Active.GetOrCreateManager<EntityManager>();

        //创建基础组件的原型
        entityArchetype = entityManager.CreateArchetype(typeof(Position), typeof(Rotation), typeof(RotationSpeed));

        if (prefab)
        {
            for (int i = 0; i < createPrefabCout; i++)
            {
                //创建实体
                Entity entities = entityManager.CreateEntity(entityArchetype);

                //设置组件
                entityManager.SetComponentData(entities, new Position { Value = UnityEngine.Random.insideUnitSphere * 100 });
                entityManager.SetComponentData(entities, new Rotation { Value = quaternion.identity });
                float randSpeed = Random.Range(1, 10);
                entityManager.SetComponentData(entities, new RotationSpeed { Value = randSpeed });

                //添加并设置组件
                entityManager.AddSharedComponentData(entities, new MeshInstanceRenderer
                {
                    mesh = this.mesh,
                    material = this.material,
                });
            }
        }
    }
}
