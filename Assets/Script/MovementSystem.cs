﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
//System是唯一继承逻辑的地方
public class MovementSystem : JobComponentSystem
{

    [BurstCompile]
    struct MovementJob : IJobProcessComponentData<Position,Rotation,MovementData>
    {
        /// <summary>
        /// 中心点及半径数据
        /// </summary>
        public quaternion targetRotation;
        public float deltaTime;
        public void Execute(ref Position position, ref Rotation rotation,ref MovementData data)
        {
            float theta = data.theta+ data.omega* deltaTime;
            float x = data.center.x + data.radius* Mathf.Cos(theta)*Mathf.Sin(data.p);
            float y = data.center.y+ data.radius *Mathf.Cos(data.p);
            float z = data.center.z + data.radius * Mathf.Sin(theta) * Mathf.Sin(data.p);
            position = new Position
            {
                Value = new float3(x, y, z)
            };
            rotation = new Rotation
            {
                Value = targetRotation
            };
            data = new MovementData
            {
                theta=theta,
                center = data.center,
                radius = data.radius,
                omega = data.omega,
                p=data.p
            };
        }
    }

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        MovementJob moveJob = new MovementJob
        {
            deltaTime = Time.deltaTime,
            targetRotation = Quaternion.LookRotation(Camera.main.transform.up)
        };
        
        moveJob.deltaTime = Time.deltaTime;
        JobHandle jobHandle = moveJob.Schedule(this, inputDeps);
        return jobHandle;
    }
}